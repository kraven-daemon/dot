# Chapter 1

openssh 

### Generate key 

> Create a private and public key in ~/.ssh/id_* 

```
ssh-keygen -t [encryption-algo] -C "labelname some people put email"
```
- then choose location
- and enter a passphrase

> Change password on a key
```
ssh-keygen -p -f ~/ssh/id_*key*
```

### Automatic authentication on login/xsession

> Using ssh-agent 

``` eval $(ssh-agent) # in .profile 
# or in 

```

### Test connection to an ssh host

```
ssh -T git@github.com


### Managing keys and signatures

> The keychain program, for ssh keys and gpg sig

funtoo is a gentoo distro on edge
[wiki on keychain here](funtoo.org/Keychain)

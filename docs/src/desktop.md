
Fonts
- ~/.local/share/fonts

Wallpapers
- ~/.local/share/backgrounds

Icons
- ~/.local/share/icons

GTK
- ~/.local/share/themes
- ~/.config/gtk-2.0
- ~/.config/gtk-3.0


Nix
- /nix

Nvim
- pkgs: langage servers[ bash-language-server, ]


# Github/Gitlab/Gitea

## Subtree/Submodule

subtree : a copy of a repository that is pulled into a parent repository,
submodule : like a pointer to a specific commit in another repository.

When to use submodule over subtree:
    - Smaller repo size vs entire repo tree
    - Centralized(server) vs Decentralized(file-based)
    - Component based development vs System based development
    - Easier to push vs Harder to push
    - Harder to pull vs Very easy to pull
    - if you dont need to update quite often

## Submodule

### To pull submodule src
git submodule init
git submodule update

OR

git clone --recurse-submodules https://git.suckless.org/dmenu

OR

git clone 'repo'
and
git submodule update --init --recursive

UPDATE
cd into submodule
git fetch and git merge origin/branch
the upstream branch will get updated and merge into the parent repo

=========================================================================
## Subtree

*Warning*
>    git-subtree is usually on a different pkg than standalone git,
>    some distribution use `meta-pkgs` to ship it with git,
>    some other more minimalist doesn't
add the url, then add the subtree

### Adding the remote url index

git remote add `remote-name` `htts://url...`

### Pulling the subtree


git subtree add --prefix `dir-path` `remote-name` `branch` --squash

### Updating the repo

git fetch `remote-name` master
git subtree pull --prefix `path` `remote-name` master --squash


